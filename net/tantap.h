#ifndef TANTAP_H
#define TANTAP_H

#include "net/net.h"
#include "net/vhost_net.h"

#define ETH_ALEN 6
#define ETH_HLEN 14
#define ETH_P_IP (0x0800) /* Internet Protocol packet  */
#define ETH_P_ARP (0x0806) /* Address Resolution packet */
#define ETH_P_IPV6 (0x86dd)
#define ETH_P_VLAN (0x8100)
#define ETH_P_DVLAN (0x88a8)
#define ETH_P_NCSI (0x88f8)
#define ETH_P_UNKNOWN (0xffff)

#define BOOTP_SERVER 67

typedef struct TANTAPState {
    NetClientState nc;
    int fd;
    char down_script[1024];
    char down_script_arg[128];
    uint8_t buf[NET_BUFSIZE];
    bool read_poll;
    bool write_poll;
    bool using_vnet_hdr;
    bool has_ufo;
    bool enabled;
    bool can_receive;
    VHostNetState *vhost_net;
    unsigned host_vnet_hdr_len;
    Notifier exit;
    Notifier poll_notifier;
} TANTAPState;

int tantap_get_fd(NetClientState *nc);

int tantap_enable(NetClientState *nc);
int tantap_disable(NetClientState *nc);

struct vhost_net;
struct vhost_net *tantap_get_vhost_net(NetClientState *nc);

//TANTAPState *tantap_state;

#endif
